@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Panel Administrativo</h1>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Editar Post
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.update', $post->id) }}">
            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
            	<input type="hidden" name="_method" value="PATCH">
            	<div class="form-group">
            		<div class="row">
	            		<label class="col-md-3">Título</label>
	            		<div class="col-md-6">
	            			<input type="text" name="titulo" class="form-control" value="{{ $post->titulo }}">
	            		</div>
	            		<div class="clearfix"></div>
	            	</div>
            	</div>
            	<div class="form-group">
            		<div class="row">
	            		<label class="col-md-3">Contenido</label>
	            		<div class="col-md-6">
	            			<textarea name="contenido" class="form-control">{{ $post->descripcion }}</textarea>
	            		</div>
	            		<div class="clearfix"></div>
	            	</div>
            	</div>
            	<div class="form-group">
            		<div class="row">
	            		<label class="col-md-3">Estatus</label>
	            		<div class="col-md-6">
	            			<select name="estatus" class="form-control">
	            				<option value="">Seleciona</option>
	            				<option value="Publicado" @if($post->status == 'Publicado') selected  @endif>Publicado</option>
	            				<option value="Borrador" @if($post->status == 'Borrador') selected  @endif>Borrador</option>
	            				<option value="Inactivo" @if($post->status == 'Inactivo') selected  @endif>Inactivo</option>
	            			</select>
	            		</div>
	            		<div class="clearfix"></div>
	            	</div>
            	</div>
            	<div class="form-group">
            		<center>
	            		<input type="submit" class="btn btn-info" value="Guardar">
	            	</center>
            	</div>
            </form>
        </div>
    </div>
</div>
@endsection
