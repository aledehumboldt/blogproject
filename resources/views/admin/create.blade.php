@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Panel Administrativo</h1>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Agregar Post
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.store') }}">
            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
            	<input type="hidden" name="usuario" value="{{ Auth::user()->name }}">
            	<div class="form-group">
            		<div class="row">
	            		<label class="col-md-3">Título</label>
	            		<div class="col-md-6">
	            			<input type="text" name="titulo" class="form-control">
	            		</div>
	            		<div class="clearfix"></div>
	            	</div>
            	</div>
            	<div class="form-group">
            		<div class="row">
	            		<label class="col-md-3">Contenido</label>
	            		<div class="col-md-6">
	            			<textarea name="contenido" class="form-control"></textarea>
	            		</div>
	            		<div class="clearfix"></div>
	            	</div>
            	</div>
            	<div class="form-group">
            		<div class="row">
	            		<label class="col-md-3">Estatus</label>
	            		<div class="col-md-6">
	            			<select name="estatus" class="form-control">
	            				<option value="">Seleciona</option>
	            				<option value="Publicado">Publicado</option>
	            				<option value="Borrador">Borrador</option>
	            				<option value="Inactivo">Inactivo</option>
	            			</select>
	            		</div>
	            		<div class="clearfix"></div>
	            	</div>
            	</div>
            	<div class="form-group">
            		<center>
	            		<input type="submit" class="btn btn-info" value="Guardar">
	            	</center>
            	</div>
            </form>
        </div>
    </div>
</div>
@endsection
