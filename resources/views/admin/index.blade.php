@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Panel Administrativo</h1>
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card bg-success text-white mb-4">
                <div class="card-body">{{ $pub }} Posts Publicados</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Ver Detalles</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-warning text-white mb-4">
                <div class="card-body">{{ $bor }} Posts Borradores</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Ver Detalles</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-danger text-white mb-4">
                <div class="card-body">{{ $ina }} Posts Inactivos</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Ver Detalles</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Tus Posts
        	<a href="{{ route('admin.create') }}" class="btn btn-primary" style="display: inline; float: right;">Agregar Post</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Contenido</th>
                            <th>Estatus</th>
                            <th>Fecha de Creación</th>
                            <th>Última Actualización</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Título</th>
                            <th>Contenido</th>
                            <th>Estatus</th>
                            <th>Fecha de Creación</th>
                            <th>Última Actualización</th>
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    	@foreach($posts as $s)
                        <tr>
                            <td>{{ $s->titulo }}</td>
                            <td>{{ $s->descripcion }}</td>
                            <td>{{ $s->status }}</td>
                            <td>{{ $s->created_at }}</td>
                            <td>{{ $s->updated_at }}</td>
                            <td width="16%">
                            	<div width="100%" align="center">
                                    <a href="{{ route('admin.edit', $s->id) }}">
                                        <img src="{{ asset('src/edit.png') }}" title="Editar" width="25%" height="25%">
                                    </a>
                                    <a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()">
                                            <img src="{{ asset('src/delete.png') }}" title="Eliminar" width="25%" height="25%">
                                        </a>
                                    <form method="post" action="{{ route('admin.destroy', $s->id) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @method('DELETE')
                                    </form>
                            	</div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
