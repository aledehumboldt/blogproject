@extends('layouts.app')

@section('imagen', 'background-image: url("../img/confirm-bg.jpg")')

@section('tittle', 'Verificar Correo')

@section('descripcion')
¡De esta manera siempre estaremos en contacto!
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verificación de Correo') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert"> {{ __('Un enlace de verificacion ha sido enviado a tu correo.') }}
                        </div>
                    @endif
 {{ __('Antes de seguir, por favor ubica en tu correo el enlace de verificación.') }} {{ __('Si no recibiste el correo') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('haz click aquí para solicitar otro') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
