@extends('layouts.app')

@section('imagen', 'background-image: url("../img/post-bg.jpg")')

@section('tittle')
{{ $post->titulo }}
@endsection

@section('descripcion')
{{ $post->descripcion }}
@endsection

@section('autorFecha')
<span class="meta">{{ $post->status }}
	el {{ date_format($post->created_at,'d F Y') }}</span>
@endsection

@section('content')

<article>
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      <p>Nunca en toda su historia los hombres han podido concebir verdaderamente el mundo como uno: una sola esfera, un globo, que tiene las cualidades de un globo, una tierra redonda en la que todas las direcciones finalmente se encuentran, en la que no hay centro porque cada punto, o ninguno, es el centro, una tierra igual que todos los hombres ocupan como iguales. La tierra del aviador, si los hombres libres lo logran, será verdaderamente redonda: un globo en la práctica, no en teoría.</p>

      <p>La ciencia tiene dos caminos, por supuesto; sus productos pueden utilizarse tanto para el bien como para el mal. Pero no hay vuelta atrás de la ciencia. Las primeras advertencias sobre peligros tecnológicos también provienen de la ciencia.</p>

      <p>Lo más significativo del viaje lunar no fue que el hombre pusiera un pie en la Luna, sino que puso el ojo en la Tierra.</p>

      <p>Un cuento chino habla de algunos hombres enviados para dañar a una joven que, al ver su belleza, se convierten en sus protectores más que en sus violadores. Así me sentí al ver la Tierra por primera vez. No pude evitar amarla y apreciarla.</p>

      <p>Para aquellos que han visto la Tierra desde el espacio, y para los cientos y quizás miles más que lo harán, la experiencia ciertamente cambia su perspectiva. Las cosas que compartimos en nuestro mundo son mucho más valiosas que las que nos dividen.</p>

      <h2 class="section-heading">La Última Frontera</h2>

      <p>No se puede pensar en terminar por "apuntar a las estrellas". Tanto en sentido figurado como literal, es una tarea para ocupar las generaciones. Y no importa cuánto avance uno, siempre existe la emoción de comenzar.</p>

      <p>No puede haber pensamientos que terminen en "apuntar a las estrellas". Tanto en sentido figurado como literal, es una tarea para ocupar las generaciones. Y no importa cuánto avance uno, siempre existe la emoción de comenzar.</p>

      <blockquote class="blockquote">Los sueños de ayer son las esperanzas de hoy y la realidad de mañana. La ciencia aún no domina la profecía. Predecimos demasiado para el próximo año y, sin embargo, demasiado poco para los próximos diez.</blockquote>

      <p>Los vuelos espaciales no se pueden detener. Este no es el trabajo de un solo hombre o incluso de un grupo de hombres. Es un proceso histórico que la humanidad está llevando a cabo de acuerdo con las leyes naturales del desarrollo humano.</p>

      <h2 class="section-heading">Alcanzando las estrellas</h2>

      <p>A medida que nos alejábamos más y más, [de la Tierra] disminuía su tamaño. Finalmente se redujo al tamaño de una canica, la más hermosa que puedas imaginar. Ese objeto hermoso, cálido y vivo se veía tan frágil, tan delicado, que si lo tocabas con un dedo se desmoronaba y se deshacía. Ver esto tiene que cambiar a un hombre.</p>

      <a href="#">
        <img class="img-fluid" src="../img/post-sample-image.jpg" alt="">
      </a>
      <span class="caption text-muted">Ir a lugares y hacer cosas que nunca se han hecho antes – de eso se trata la vida.</span>

      <p>El espacio, la última frontera. Estos son los viajes del Starship Enterprise. Su misión de cinco años: explorar extraños mundos nuevos, buscar nueva vida y nuevas civilizaciones, ir audazmente a donde ningún hombre ha ido antes.</p>

      <p>Mientras me paro aquí en las maravillas de lo desconocido en Hadley, me doy cuenta de que hay una verdad fundamental en nuestra naturaleza, el hombre debe explorar, y esta es la exploración en su máxima expresión.</p>
    </div>
  </div>
  </article>

@endsection