@extends('layouts.app')

@section('imagen', 'background-image: url("img/home-bg.jpg")')

@section('tittle', 'Blog Libre')

@section('descripcion')
¡Siéntete libre de expresarte como desees!
@endsection

@section('content')
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        @foreach($posts as $s)
        <div class="post-preview">
          <a href="{{ route('postdetail', $s->id) }}">
            <h2 class="post-title">
              {{ $s->titulo }}
            </h2>
            <h3 class="post-subtitle">
              {{ $s->descripcion }}
            </h3>
          </a>
          <p class="post-meta">{{ $s->status }}
            el {{ date_format($s->created_at,'d F Y') }}</p>
        </div>
        <hr>
        @endforeach
        <!-- Pager -->
        <div class="clearfix">
        @if($posts->hasPages())
          @if ($posts->onFirstPage())
            @if ($posts->hasMorePages())
              <a href="{{ $posts->nextPageUrl() }}" class="btn btn-primary float-right">Posts más viejos &rarr;</a>
            @endif
          @else
            <a href="{{ $posts->previousPageUrl() }}" class="btn btn-primary float-left">&larr; Posts más nuevos</a>
            @if ($posts->hasMorePages())
              <a href="{{ $posts->nextPageUrl() }}" class="btn btn-primary float-right">Posts más viejos &rarr;</a>
            @endif
          @endif
        @endif
        </div>
      </div>
    </div>
@endsection
