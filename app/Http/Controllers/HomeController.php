<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about() {
        return view('about');
    }
    public function post() {
        return view('post');
    }
    public function detail($id) {
        $arr['post'] = Post::find($id);
        return view('post')->with($arr);
    }
}
