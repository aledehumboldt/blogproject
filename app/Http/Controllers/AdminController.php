<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;

class AdminController extends Controller {
    
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    	$user_id = auth()->user()->id;

     	$pub = Post::where('status', 'Publicado')
            ->where('user_id', $user_id)->count();
        $bor = Post::where('status', 'Borrador')
            ->where('user_id', $user_id)->count();
        $ina = Post::where('status', 'Inacctivo')
            ->where('user_id', $user_id)->count();

        $arr['posts'] = Post::where('user_id', $user_id)->get();
        $arr['pub'] = $pub;
        $arr['bor'] = $bor;
        $arr['ina'] = $ina;

     	// print_r($arr);
     	return view('admin.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post) {
        $post->titulo = $request->titulo;
        $post->descripcion = $request->contenido;
        $post->status = $request->estatus;
        $post->user_id = auth()->user()->id;
        
        $post->save();
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::find($id);
        $arr['post'] = $post;
        return view('admin.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
    	$post = Post::find($id);
        $post->titulo = $request->titulo;
        $post->descripcion = $request->contenido;
        $post->status = $request->estatus;
        $post->save();
        return redirect('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Post::destroy($id);
        return redirect('admin');
    }
}

	