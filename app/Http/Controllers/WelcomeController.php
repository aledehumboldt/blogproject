<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class WelcomeController extends Controller {
    public function index() {
        if (Auth::check()) {
            $arr['posts'] = Post::where('status', 'Publicado')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

            return view('home')->with($arr);
            die();
        }
        return view('welcome');
    }
}
